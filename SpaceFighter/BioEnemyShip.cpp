
#include "BioEnemyShip.h"


BioEnemyShip::BioEnemyShip()
{
	SetSpeed(150); // Changes the enemy ships speed. seems to affect all enemys, so all enemies must be bio ships? 
	SetMaxHitPoints(1); // how much health the ship has, if increased will require that many more hits to kill
	SetCollisionRadius(20); // the "size" of the ship, should be very similar to the ship, if increased will make it so you die without touching them
}


void BioEnemyShip::Update(const GameTime *pGameTime)
{
	if (IsActive()) //if the ship is active, this one is pretty clera
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex()); // gets the sin of pi plus a couple of other values, index which returns s_count from gameobject.cpp
		x *= 0;//GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f; // currently changed to remove arc by setting to 0 so all lower values are 0;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed()); //seems to move the ship based on speed and the time to get the arc it travels

		if (!IsOnScreen()) Deactivate(); // if no longer on the screen, deactivates the current enempy
	}

	EnemyShip::Update(pGameTime);
}


void BioEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
