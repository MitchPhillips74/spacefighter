
#include "Projectile.h"

Texture *Projectile::s_pTexture = nullptr;

Projectile::Projectile() // EX Pure Virtual
{
	SetSpeed(500);
	SetDamage(1);
	SetDirection(-Vector2::UNIT_Y);
	SetCollisionRadius(9);

	m_drawnByLevel = true;
}

void Projectile::Update(const GameTime *pGameTime)
{
	if (IsActive())
	{
		Vector2 translation = m_direction * m_speed * pGameTime->GetTimeElapsed(); // figures out what it should do each time its moving for future calling
		TranslatePosition(translation); // actually moves it in the direction and speed set before.

		Vector2 position = GetPosition(); // returns the position on the screen for the projectle
		Vector2 size = s_pTexture->GetSize(); // gets the size of the texture to make sure the whole thing is off the screen before removing 

		// Is the projectile off the screen?
		if (position.Y < -size.Y) Deactivate(); 
		else if (position.X < -size.X) Deactivate(); // if its off the side of the screen complely
		else if (position.Y > Game::GetScreenHeight() + size.Y) Deactivate(); // if its farther down than the screen is tall then remove
		else if (position.X > Game::GetScreenWidth() + size.X) Deactivate(); // if its farther off to the side than the screen is wide then remove
	}

	GameObject::Update(pGameTime);
}

void Projectile::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(s_pTexture, GetPosition(), Color::White, s_pTexture->GetCenter());
	}
}

void Projectile::Activate(const Vector2 &position, bool wasShotByPlayer)
{
	m_wasShotByPlayer = wasShotByPlayer;
	SetPosition(position);

	GameObject::Activate();
}

std::string Projectile::ToString() const
{
	return ((WasShotByPlayer()) ? "Player " : "Enemy ") + GetProjectileTypeString();
}

CollisionType Projectile::GetCollisionType() const
{
	CollisionType shipType = WasShotByPlayer() ? CollisionType::PLAYER : CollisionType::ENEMY;
	return (shipType | GetProjectileType());
}